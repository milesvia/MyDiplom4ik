package com.iryzhova.mydiplom4ik.ui.chat.teteatete;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.iryzhova.mydiplom4ik.R;
import com.iryzhova.mydiplom4ik.adapter.teteatetechat.AdapterChat;
import com.iryzhova.mydiplom4ik.pojo.Contact;
import com.iryzhova.mydiplom4ik.pojo.ModelChat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.iryzhova.mydiplom4ik.ui.chat.teteatete.MessengerFragment.helpViewChat;

public class ChatThisUserFragment {
    FirebaseDatabase firebaseDatabase;
    DatabaseReference userDbRef;
    FirebaseAuth firebaseAuth;
    public static ValueEventListener seenListener;
    public static DatabaseReference userRefSeen;
    RecyclerView recyclerView;

    List<ModelChat> chatList;
    AdapterChat adapterChat;

    private Context context;
    private Contact companion;
    EditText editTextSendMess;
    TextView textView_nameCompanionChat, textView_emailCompanionChat;
    ImageView imageView_sendMess;
    private String thisUidCopmanion="";
    private String myUid="";


    private void initChat(){
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(context);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    @SuppressLint("ResourceAsColor")
    public ChatThisUserFragment(Contact companion, Context context){
        this.companion=companion;
        this.context=context;
        editTextSendMess=helpViewChat.findViewById(R.id.EditText_sendMess);
        textView_emailCompanionChat=helpViewChat.findViewById(R.id.TextView_emailCompanionChat);
        textView_nameCompanionChat=helpViewChat.findViewById(R.id.TextView_nameCompanionChat);
        imageView_sendMess=helpViewChat.findViewById(R.id.ImageView_sendMess);
        recyclerView=helpViewChat.findViewById(R.id.id_recycler_chat);
        if(imageView_sendMess!=null){
            imageView_sendMess.setOnClickListener(v->{
                String message=editTextSendMess.getText().toString();
                if(TextUtils.isEmpty(message)){
                    //message empty
                }else{
                    sendMessage(message);
                }

            });

            readMessages();
            seenMessage();
        

        }
        if(editTextSendMess!=null) {
            thisUidCopmanion=companion.getUid();
            editTextSendMess.setHint("Input message");
            textView_nameCompanionChat.setText(companion.getName());
            textView_emailCompanionChat.setText(companion.getEmail());
        }

        initChat();

        firebaseAuth=FirebaseAuth.getInstance();
        firebaseDatabase=FirebaseDatabase.getInstance();
        userDbRef=firebaseDatabase.getReference("Users");
        myUid=FirebaseAuth.getInstance().getCurrentUser().getUid();


        Query userQuery=userDbRef.orderByChild("uid").equalTo(thisUidCopmanion);
        userQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot ds: snapshot.getChildren()){
                    String name=""+ds.child("name").getValue();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void seenMessage() {
        userRefSeen=FirebaseDatabase.getInstance().getReference("Chats");
        seenListener=userRefSeen.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot ds: snapshot.getChildren()){
                    ModelChat chat=ds.getValue(ModelChat.class);
                    if(chat.getReceiver().equals(myUid) && chat.getSender().equals(thisUidCopmanion)){
                        HashMap<String, Object> hasSeenHashMap=new HashMap<>();
                        hasSeenHashMap.put("isSeen", true);
                        ds.getRef().updateChildren(hasSeenHashMap);
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void readMessages() {
        chatList=new ArrayList<>();
        DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference("Chats");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                chatList.clear();
                for(DataSnapshot ds: snapshot.getChildren()){
                    ModelChat chat=ds.getValue(ModelChat.class);
                    if(chat.getReceiver().equals(myUid)
                            &&  chat.getSender().equals(thisUidCopmanion)
                        || chat.getSender().equals(myUid)
                            && chat.getReceiver().equals(thisUidCopmanion)){
                        chatList.add(chat);
                    }
                    adapterChat=new AdapterChat(context, chatList);
                    adapterChat.notifyDataSetChanged();
                    recyclerView.setAdapter(adapterChat);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void sendMessage(String message) {
        DatabaseReference databaseReference=FirebaseDatabase.getInstance().getReference();

        String timetamp=String.valueOf(System.currentTimeMillis());
        HashMap<String, Object> hashMap=new HashMap<>();
        hashMap.put("sender", myUid);
        hashMap.put("receiver",thisUidCopmanion);
        hashMap.put("message", message);
        hashMap.put("timestamp", timetamp);
        hashMap.put("isSeen", false);
        databaseReference.child("Chats").push().setValue(hashMap);
        editTextSendMess.setText("");
    }

}
