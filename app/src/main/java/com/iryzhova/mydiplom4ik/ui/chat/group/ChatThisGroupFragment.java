package com.iryzhova.mydiplom4ik.ui.chat.group;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iryzhova.mydiplom4ik.R;
import com.iryzhova.mydiplom4ik.adapter.conference.AdapterConference;
import com.iryzhova.mydiplom4ik.adapter.teteatetechat.AdapterChat;
import com.iryzhova.mydiplom4ik.alertDialogs.ALertDialogSettingsConference;
import com.iryzhova.mydiplom4ik.alertDialogs.AlertDialogAddGroupChat;
import com.iryzhova.mydiplom4ik.pojo.Group;
import com.iryzhova.mydiplom4ik.pojo.ModelGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.iryzhova.mydiplom4ik.ui.chat.group.GroupsChatFragment.helpViewGroupChat;

public class ChatThisGroupFragment {
    FirebaseDatabase firebaseDatabase;
    DatabaseReference userDbRef;
    FirebaseAuth firebaseAuth;
    public static ValueEventListener seenListener;
    public static DatabaseReference groupRefSeen;
    RecyclerView recyclerView;

    List<ModelGroup> chatList;
    AdapterConference adapterConference;

    private Context context;
    private Group group;
    EditText editTextSendMess;
    TextView textView_nameCompanionChat, textView_emailCompanionChat;
    ImageView imageView_sendMess;
    RelativeLayout infoCompanionGroup;
    private String thisUidGroup="";
    private String thisName="";
    private String myUid="";

    private ALertDialogSettingsConference aLertDialogSettingsConference;


    private void initChat(){
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(context);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    @SuppressLint("ResourceAsColor")
    public ChatThisGroupFragment(Group group, Context context){
        this.group=group;
        this.context=context;
        editTextSendMess=helpViewGroupChat.findViewById(R.id.EditText_sendMess);
        textView_emailCompanionChat=helpViewGroupChat.findViewById(R.id.TextView_emailCompanionChat);
        textView_nameCompanionChat=helpViewGroupChat.findViewById(R.id.TextView_nameCompanionChat);
        imageView_sendMess=helpViewGroupChat.findViewById(R.id.ImageView_sendMess);
        recyclerView=helpViewGroupChat.findViewById(R.id.id_recycler_chat);
        infoCompanionGroup=helpViewGroupChat.findViewById(R.id.infoCompanionGroup);
        if(imageView_sendMess!=null){
            imageView_sendMess.setOnClickListener(v->{
                String message=editTextSendMess.getText().toString();
                if(TextUtils.isEmpty(message)){
                    //message empty
                }else{
                    sendMessage(message);
                }

            });

            readMessages();
            seenMessage();
        }
        if(editTextSendMess!=null) {
            thisUidGroup=group.getGroupId();

            editTextSendMess.setHint("Input message");
            textView_nameCompanionChat.setText(group.getGroupTitle());
            textView_emailCompanionChat.setText(group.getGroupDescription());
        }

        initChat();
        aLertDialogSettingsConference=new ALertDialogSettingsConference(context);

        firebaseAuth=FirebaseAuth.getInstance();
        firebaseDatabase=FirebaseDatabase.getInstance();
        userDbRef=firebaseDatabase.getReference("GroupsChat");
        myUid=FirebaseAuth.getInstance().getCurrentUser().getUid();
        thisName=FirebaseAuth.getInstance().getCurrentUser().getEmail();

        infoCompanionGroup.setOnClickListener(v->{
            aLertDialogSettingsConference.showALertDialogSettingsConference();
        });
        /*Query userQuery=userDbRef.orderByChild("uid").equalTo(thisUidCopmanion);
        userQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot ds: snapshot.getChildren()){
                    String name=""+ds.child("name").getValue();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });*/
    }

    private void seenMessage() {
        groupRefSeen=FirebaseDatabase.getInstance().getReference("GroupsChat");
        seenListener=groupRefSeen.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot ds: snapshot.getChildren()){
                    ModelGroup chatGoup=ds.getValue(ModelGroup.class);
                    assert chatGoup != null;
                    if(chatGoup.getReceiverGroup().equals(myUid) && chatGoup.getSender().equals(thisUidGroup)){
                        HashMap<String, Object> hasSeenHashMap=new HashMap<>();
                        hasSeenHashMap.put("isSeen", true);
                        ds.getRef().updateChildren(hasSeenHashMap);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void readMessages() {
        chatList=new ArrayList<>();
        DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference("GroupsChat");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                chatList.clear();
                for(DataSnapshot ds: snapshot.getChildren()){
                    ModelGroup chatGroup=ds.getValue(ModelGroup.class);
                    if( chatGroup.getReceiverGroup().equals(thisUidGroup)){
                        chatList.add(chatGroup);
                    }
                    adapterConference=new AdapterConference(context, chatList);
                    adapterConference.notifyDataSetChanged();
                    recyclerView.setAdapter(adapterConference);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void sendMessage(String message) {
       DatabaseReference databaseReference=FirebaseDatabase.getInstance().getReference();

        String timetamp=String.valueOf(System.currentTimeMillis());
        HashMap<String, Object> hashMap=new HashMap<>();
        hashMap.put("sender", myUid);
        hashMap.put("receiverGroup",thisUidGroup);
        hashMap.put("message", message);
        hashMap.put("nameSender", thisName);
        hashMap.put("timestamp", timetamp);
        hashMap.put("isSeen", false);
        databaseReference.child("GroupsChat").push().setValue(hashMap);
        editTextSendMess.setText("");
    }
}
