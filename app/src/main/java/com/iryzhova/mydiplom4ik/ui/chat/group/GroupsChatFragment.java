package com.iryzhova.mydiplom4ik.ui.chat.group;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iryzhova.mydiplom4ik.R;
import com.iryzhova.mydiplom4ik.adapter.conference.ConferenceListAdapter;
import com.iryzhova.mydiplom4ik.adapter.teteatetechat.ContactsListAdapter;
import com.iryzhova.mydiplom4ik.alertDialogs.AlertDialogAddGroupChat;
import com.iryzhova.mydiplom4ik.pojo.Contact;
import com.iryzhova.mydiplom4ik.pojo.Group;

import java.util.ArrayList;
import java.util.List;


public class GroupsChatFragment extends Fragment {
    private FirebaseAuth firebaseAuth;

    private ConferenceListAdapter adapter;
    private RecyclerView recyclerView;
    public static View helpViewGroupChat;
    private AppCompatButton buttonCreateGroup;
    private AlertDialogAddGroupChat alertDialogGroupChat;
    @RequiresApi(api = Build.VERSION_CODES.N)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_group_chat, container, false);
        helpViewGroupChat = root;
        buttonCreateGroup=root.findViewById(R.id.button_create_group);
        alertDialogGroupChat=new AlertDialogAddGroupChat(getContext());

        buttonCreateGroup.setOnClickListener(v->{alertDialogGroupChat.showAlertDialogCreateNewGroup();});

        recyclerView=root.findViewById(R.id.id_recycler_groups);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        firebaseAuth=firebaseAuth.getInstance();
        getAllGroups();
        return root;
    }

    private void getAllGroups() {
        ArrayList<Group> conference = new ArrayList<>();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Groups");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                conference.clear();
                for (DataSnapshot ds : snapshot.getChildren()) {
                    Group group = ds.getValue(Group.class);
                    List<String> uidMembers=group.getListUoidMembers(group.getUidMembers());
                    if(uidMembers.contains(firebaseAuth.getCurrentUser().getUid()))
                        conference.add(group);
                }
                adapter = new ConferenceListAdapter(getContext(),conference);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void checkUser() {
        FirebaseUser user=firebaseAuth.getCurrentUser();


       /* if(user!=null){
            actionBar.setSubtitle(user.getEmail());
        }*/
        //Toast.makeText(getApplication(), "Meow", Toast.LENGTH_LONG).show();
    }
}
