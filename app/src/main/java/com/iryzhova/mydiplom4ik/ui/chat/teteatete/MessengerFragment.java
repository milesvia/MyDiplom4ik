package com.iryzhova.mydiplom4ik.ui.chat.teteatete;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iryzhova.mydiplom4ik.R;
import com.iryzhova.mydiplom4ik.adapter.teteatetechat.ContactsListAdapter;
import com.iryzhova.mydiplom4ik.pojo.Contact;

import java.util.ArrayList;

public class MessengerFragment extends Fragment {

    //private ChatViewModel chatViewModel;
    private ContactsListAdapter adapter;
    private RecyclerView recyclerView;

    public static View helpViewChat;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        /*chatViewModel =
                new ViewModelProvider(this).get(ChatViewModel.class);*/
        View root = inflater.inflate(R.layout.fragment_chat, container, false);
        helpViewChat=root;
        //final TextView textView = root.findViewById(R.id.text_chatView);
        recyclerView=root.findViewById(R.id.id_recycler_contacts);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        
        /*chatViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        getAllUsers();
        return root;
    }

    @Override
    public void onPause() {
        super.onPause();
        //userRefSeen.removeEventListener(seenListener);
    }

    private void getAllUsers() {
        ArrayList<Contact> contactsName = new ArrayList<>();
        FirebaseUser fUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                contactsName.clear();
                for (DataSnapshot ds : snapshot.getChildren()) {
                    Contact contact = ds.getValue(Contact.class);
                    if (!contact.getUid().equals(fUser.getUid())) {
                        contactsName.add(contact);
                    }
                }
                adapter = new ContactsListAdapter(getContext(), contactsName);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}