package com.iryzhova.mydiplom4ik.pojo;

import com.google.firebase.database.PropertyName;

public class ModelGroup {
    String message;
    String receiverGroup;
    String sender;
    String nameSender;

    String timestamp;
    boolean isSeen;

    public ModelGroup(){}

    public ModelGroup(String message, String receiverGroup, String sender, String timestamp, boolean isSeen){
        this.message=message;
        this.receiverGroup=receiverGroup;
        this.sender=sender;
        this.timestamp=timestamp;
        this.isSeen=isSeen;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReceiverGroup() {
        return receiverGroup;
    }

    public void setReceiverGroup(String receiverGroup) {
        this.receiverGroup = receiverGroup;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


    @PropertyName("isSeen")
    public boolean isSeen() {
        return isSeen;
    }

    @PropertyName("isSeen")
    public void setSeen(boolean seen) {
        isSeen = seen;
    }

    public String getNameSender() {
        return nameSender;
    }

    public void setNameSender(String nameSender) {
        this.nameSender = nameSender;
    }
}
