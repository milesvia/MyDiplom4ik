package com.iryzhova.mydiplom4ik.pojo;

public class ItemNavigation {
    public int icon;
    public String name;

    // модель данных используемая в адаптере DrawerItemCustomAdapter
    public ItemNavigation(int icon, String name) {
        this.icon = icon;
        this.name = name;
    }
}

