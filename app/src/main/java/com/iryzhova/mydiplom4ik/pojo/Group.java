package com.iryzhova.mydiplom4ik.pojo;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Group {
    String groupId;
    String groupTitle;
    String groupDescription;
    String groupIcon;
    String timestamp;
    String createBy;

    String uidMembers;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupTitle() {
        return groupTitle;
    }

    public void setGroupTitle(String groupTitle) {
        this.groupTitle = groupTitle;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public String getGroupIcon() {
        return groupIcon;
    }

    public void setGroupIcon(String groupIcon) {
        this.groupIcon = groupIcon;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUidMembers() {
        return uidMembers;
    }

    public void setUidMembers(String uidMembers) {
        this.uidMembers = uidMembers;
    }

    public List<String> getListUoidMembers(String listUid){
        return Arrays.asList(listUid.split(","));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public String setStringUidMembers(List<String> uidMembers){
        return uidMembers.stream().collect(Collectors.joining(","));
    }


    public Group(){}

}
