package com.iryzhova.mydiplom4ik.auth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.iryzhova.mydiplom4ik.R;
import com.iryzhova.mydiplom4ik.StartPage;
import com.iryzhova.mydiplom4ik.UserProfile;

public class Authorization extends AppCompatActivity {
    private EditText mEditTextPasswordA, mEditTextLoginA;
    private ProgressDialog progressDialog;
    private MaterialButton mButtonAuthA;
    private TextView textViewForgettenPasswordA;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        setContentView(R.layout.activity_authorization);

        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        mEditTextPasswordA=findViewById(R.id.editTextPasswordA);
        mEditTextLoginA=findViewById(R.id.editTextLoginA);
        mButtonAuthA=findViewById(R.id.buttonAuthA);
        mButtonAuthA.setOnClickListener(v->{
            String email=mEditTextLoginA.getText().toString().trim();
            String password=mEditTextPasswordA.getText().toString().trim();

            if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                mEditTextLoginA.setError("Invalid");
                mEditTextLoginA.setFocusable(true);
            }
            else if (password.length()<6){
                mEditTextPasswordA.setError("Very short");
                mEditTextPasswordA.setFocusable(true);
            }
            else{
                authUser(email, password);
            }
        });

        textViewForgettenPasswordA=findViewById(R.id.TextViewForgettenPasswordA);
        textViewForgettenPasswordA.setOnClickListener(v->{
            showRecoverPasswordDialog();
        });
    }

    private void showRecoverPasswordDialog() {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Recover Password");
        LinearLayout linearLayout=new LinearLayout(this);
        final EditText loginUser=new EditText(this);
        loginUser.setHint("Email");
        loginUser.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        loginUser.setMinEms(16);

        linearLayout.addView(loginUser);
        linearLayout.setPadding(10,10,10,10);

        builder.setView(linearLayout);

        builder.setPositiveButton("Recover", (dialog, which) -> {
            String login=loginUser.getText().toString().trim();
            beginRecovery(login);

        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
        builder.create().show();
    }

    private void beginRecovery(String login){
        progressDialog.setMessage("Sending email...");
        mAuth.sendPasswordResetEmail(login)
                .addOnCompleteListener(task -> {
                    progressDialog.dismiss();
                    if (task.isSuccessful()) {
                        Toast.makeText(Authorization.this, "Email sent", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(Authorization.this, "Failed", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(e -> {
                    progressDialog.dismiss();
                    Toast.makeText(Authorization.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
        });

    }

    private void authUser(String email, String password) {
        progressDialog.show();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        progressDialog.dismiss();
                        // Sign in success, update UI with the signed-in user's information
                        Toast.makeText(Authorization.this, "Successful authentication",
                                Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(Authorization.this, StartPage.class));
                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(Authorization.this, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(e -> {
                    progressDialog.dismiss();
                    Toast.makeText(Authorization.this, ""+e.getMessage(), Toast.LENGTH_LONG).show();
                });
    }
}