package com.iryzhova.mydiplom4ik.auth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.iryzhova.mydiplom4ik.R;
import com.iryzhova.mydiplom4ik.StartPage;
import com.iryzhova.mydiplom4ik.UserProfile;

import java.util.HashMap;
import java.util.regex.Pattern;

public class Registration extends AppCompatActivity {

    private EditText mEditTextPasswordR, mEditTextLoginR, mEditTextNameR;
    private MaterialButton mButtonRegistR;

    ProgressDialog progressDialog;


    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        mAuth = FirebaseAuth.getInstance();

        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        mEditTextPasswordR=findViewById(R.id.editTextPasswordR);
        mEditTextLoginR=findViewById(R.id.editTextLoginR);
        mEditTextNameR=findViewById(R.id.editTextNameR);
        mButtonRegistR=findViewById(R.id.buttonRegistR);
        mButtonRegistR.setOnClickListener(v->{
            String email=mEditTextLoginR.getText().toString().trim();
            String password=mEditTextPasswordR.getText().toString().trim();

            if(mEditTextNameR.getText().length()<2) {
                mEditTextLoginR.setError("This name is very strange or empty");
                mEditTextLoginR.setFocusable(true);
            }
            else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                mEditTextLoginR.setError("This email is uncorrect");
                mEditTextLoginR.setFocusable(true);
            }
            else if (password.length()<6){
                mEditTextPasswordR.setError("This password is very easy");
                mEditTextPasswordR.setFocusable(true);
            }
            else{
                registerUser(email, password);
            }
        });

    }

    private void registerUser(String email, String password) {
        progressDialog.show();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser user = mAuth.getCurrentUser();
                        Toast.makeText(Registration.this, "Successful authentication",
                                Toast.LENGTH_SHORT).show();
                        String fEmail=user.getEmail();
                       String fUid=user.getUid();

                        HashMap<Object, String> hashMap=new HashMap<>();
                        hashMap.put("email", fEmail);
                        hashMap.put("name", mEditTextNameR.getText().toString());
                        hashMap.put("uid", fUid);
                        hashMap.put("image", "");


                        FirebaseDatabase database=FirebaseDatabase.getInstance();
                        DatabaseReference reference=database.getReference("Users");
                        reference.child(fUid).setValue(hashMap);

                        startActivity(new Intent(Registration.this, StartPage.class));
                        finish();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(Registration.this, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(e -> {
                    progressDialog.dismiss();
                    Toast.makeText(Registration.this, ""+e.getMessage(),Toast.LENGTH_SHORT).show();
                });

    }



}