package com.iryzhova.mydiplom4ik.adapter.conference;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.iryzhova.mydiplom4ik.R;
import com.iryzhova.mydiplom4ik.pojo.ModelGroup;

import java.util.Calendar;
import java.util.List;

public class AdapterConference extends RecyclerView.Adapter<AdapterConference.MyHolder> {
    private static final int MSG_TYPE_LEFT=0;
    private static final int MSG_TYPE_RIGHT=1;
    Context context;
    List<ModelGroup> chatGroupList;

    FirebaseUser firebaseUser;

    public AdapterConference(Context context, List<ModelGroup> chatGroupList){
        this.context=context;
        this.chatGroupList=chatGroupList;
    }

    boolean flagLeftView=false;
    @NonNull
    @Override
    public AdapterConference.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType==MSG_TYPE_LEFT){
            View view= LayoutInflater.from(context).inflate(R.layout.row_left, parent,false);
            flagLeftView=true;
            return new AdapterConference.MyHolder(view);
        }
        else {
            View view= LayoutInflater.from(context).inflate(R.layout.row_right, parent,false);
            flagLeftView=false;
            return new AdapterConference.MyHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterConference.MyHolder holder, int position) {
        String message=chatGroupList.get(position).getMessage();
        String time=chatGroupList.get(position).getTimestamp();
        String whoIs= chatGroupList.get(position).getNameSender();

        Calendar calendar=Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(time));
        String dateTime= DateFormat.format("dd/MM/yyyy  \nhh:mm aa", calendar).toString();

        holder.messageInChat.setText(message);
        holder.timeMessInChat.setText(dateTime);
        if(flagLeftView) {
            holder.textViewWhoIs.setText(whoIs);
            holder.textViewWhoIs.setVisibility(View.VISIBLE);
        }
        else  holder.textViewWhoIs.setVisibility(View.GONE);


        if (position==chatGroupList.size()-1){
            if(chatGroupList.get(position).isSeen()) {
                holder.isSeen.setText("✓✓");
            }else{
                holder.isSeen.setText("✓");
            }}else{
            holder.isSeen.setVisibility(View.GONE);

        }

    }

    @Override
    public int getItemCount() {
        return chatGroupList.size();
    }

    @Override
    public int getItemViewType(int position) {
        firebaseUser= FirebaseAuth.getInstance().getCurrentUser();
        if(chatGroupList.get(position).getSender().equals(firebaseUser.getUid())){
            return MSG_TYPE_RIGHT;
        }
        else
            return MSG_TYPE_LEFT;
    }

    class MyHolder extends RecyclerView.ViewHolder{
        TextView messageInChat, timeMessInChat, isSeen, textViewWhoIs;


        public MyHolder(@NonNull View itemView) {
            super(itemView);

            messageInChat=itemView.findViewById(R.id.message_in_chat);
            timeMessInChat=itemView.findViewById(R.id.time_message_in_chat);
            textViewWhoIs=itemView.findViewById(R.id.textview_whois);
            isSeen=itemView.findViewById(R.id.delivered_messege_in_chat);
        }
    }

}