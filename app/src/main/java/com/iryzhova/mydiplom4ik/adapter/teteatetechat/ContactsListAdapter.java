package com.iryzhova.mydiplom4ik.adapter.teteatetechat;

import android.annotation.SuppressLint;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;


import androidx.recyclerview.widget.RecyclerView;

import com.iryzhova.mydiplom4ik.R;
import com.iryzhova.mydiplom4ik.pojo.Contact;
import com.iryzhova.mydiplom4ik.ui.chat.teteatete.ChatThisUserFragment;

import java.util.ArrayList;




public class ContactsListAdapter extends  RecyclerView.Adapter<ContactsListAdapter.MyHolder> {

    Context context;
    ArrayList<Contact> contactsName;
    public ContactsListAdapter(Context context,  ArrayList<Contact> contactsName){
        this.context=context;
        this.contactsName=contactsName;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
       return new ContactsListAdapter.MyHolder(view);
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        String contactname=contactsName.get(position).getName();
        holder.name_contact.setText(contactname);
        holder.itemView.setOnClickListener(v->{
            //holder.itemView.setBackgroundColor(R.color.C4B7595_light);
            ChatThisUserFragment chatThisUserFragment=new ChatThisUserFragment(contactsName.get(position), context);
        });


    }

    @Override
    public int getItemCount() {
        return contactsName.size();
    }

    class MyHolder extends RecyclerView.ViewHolder{

        ImageView avatar_contact;
        TextView name_contact;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            avatar_contact=itemView.findViewById(R.id.imageView_avatar_contact);
            name_contact=itemView.findViewById(R.id.textView_name_contact);

        }
    }
}
