package com.iryzhova.mydiplom4ik.adapter.showUsers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iryzhova.mydiplom4ik.R;
import com.iryzhova.mydiplom4ik.pojo.Contact;

import java.util.ArrayList;

public class UsersListsAlertDialogAdapter extends  RecyclerView.Adapter<UsersListsAlertDialogAdapter.MyHolder> {
    Context context;
    ArrayList<Contact> contactsName;
    ArrayList<CheckBox> listCheckBox;

    public UsersListsAlertDialogAdapter(Context context,  ArrayList<Contact> contactsName){
        this.context=context;
        //System.out.println("--------------------->"+contactsName.size());
        this.contactsName=contactsName;
        listCheckBox=new ArrayList<>();
    }

    @NonNull
    @Override
    public UsersListsAlertDialogAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_searching_contact, parent, false);
        return new UsersListsAlertDialogAdapter.MyHolder(view);
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull UsersListsAlertDialogAdapter.MyHolder holder, int position) {
        String contactname=contactsName.get(position).getName();
        listCheckBox.add(holder.checkbox_chose_user);
        holder.text_view_search_users.setText(contactname);
        holder.checkbox_chose_user.setChecked(false);
        holder.itemView.setOnClickListener(v->{
            holder.checkbox_chose_user.setChecked(true);
        });


    }

    @Override
    public int getItemCount() {
        return contactsName.size();
    }

    public void falseAllCheckBox() {
        for(CheckBox checkBox: listCheckBox){
            checkBox.setChecked(false);
        }
    }

    public ArrayList<Contact> getChoseContact() {
        ArrayList<Contact> choseContact=new ArrayList<>();
        for(int i=0;i<contactsName.size();i++){
            if(listCheckBox.get(i).isChecked()){
                choseContact.add(contactsName.get(i));
            }
        }
        return choseContact;
    }

    class MyHolder extends RecyclerView.ViewHolder{
        CheckBox checkbox_chose_user;
        TextView text_view_search_users;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            text_view_search_users=itemView.findViewById(R.id.text_view_search_users);
            checkbox_chose_user=itemView.findViewById(R.id.checkbox_chose_user);
        }
    }
}
