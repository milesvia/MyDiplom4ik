package com.iryzhova.mydiplom4ik.adapter.conference;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iryzhova.mydiplom4ik.R;
import com.iryzhova.mydiplom4ik.adapter.teteatetechat.ContactsListAdapter;
import com.iryzhova.mydiplom4ik.pojo.Contact;
import com.iryzhova.mydiplom4ik.pojo.Group;
import com.iryzhova.mydiplom4ik.ui.chat.group.ChatThisGroupFragment;
import com.iryzhova.mydiplom4ik.ui.chat.group.GroupsChatFragment;
import com.iryzhova.mydiplom4ik.ui.chat.teteatete.ChatThisUserFragment;

import java.util.ArrayList;

public class ConferenceListAdapter extends RecyclerView.Adapter<ConferenceListAdapter.MyHolderGroup> {

    Context context;
    ArrayList<Group> groupName;
    public ConferenceListAdapter(Context context,  ArrayList<Group> groupName){
        this.context=context;
        this.groupName=groupName;
    }

    @NonNull
    @Override
    public MyHolderGroup onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
        return new MyHolderGroup(view);
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull MyHolderGroup holder, int position) {
        String grName=groupName.get(position).getGroupTitle();
        holder.name_contact.setText(grName);
        holder.itemView.setOnClickListener(v->{
            //holder.itemView.setBackgroundColor(R.color.C4B7595_light);
            ChatThisGroupFragment chatThisGroupFragment=new ChatThisGroupFragment(groupName.get(position), context);
            //GroupsChatFragment chatThisUserFragment=new GroupsChatFragment(groupName.get(position), context);
        });


    }

    @Override
    public int getItemCount() {
        return groupName.size();
    }

    class MyHolderGroup extends RecyclerView.ViewHolder{

        ImageView avatar_contact;
        TextView name_contact;

        public MyHolderGroup(@NonNull View itemView) {
            super(itemView);
            avatar_contact=itemView.findViewById(R.id.imageView_avatar_contact);
            avatar_contact.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_baseline_groups_24));
            name_contact=itemView.findViewById(R.id.textView_name_contact);

        }
    }
}
