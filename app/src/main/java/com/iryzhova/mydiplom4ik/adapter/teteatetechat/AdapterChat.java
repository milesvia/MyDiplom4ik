package com.iryzhova.mydiplom4ik.adapter.teteatetechat;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.iryzhova.mydiplom4ik.R;
import com.iryzhova.mydiplom4ik.pojo.ModelChat;

import java.util.Calendar;
import java.util.List;

public class AdapterChat extends RecyclerView.Adapter<AdapterChat.MyHolder> {
    private static final int MSG_TYPE_LEFT=0;
    private static final int MSG_TYPE_RIGHT=1;
    Context context;
    List<ModelChat> chatList;

    FirebaseUser firebaseUser;

    public AdapterChat(Context context, List<ModelChat> chatList){
        this.context=context;
        this.chatList=chatList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType==MSG_TYPE_LEFT){
            View view= LayoutInflater.from(context).inflate(R.layout.row_left, parent,false);
            return new MyHolder(view);
        }
        else {
                View view= LayoutInflater.from(context).inflate(R.layout.row_right, parent,false);
                return new MyHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        String message=chatList.get(position).getMessage();
        String time=chatList.get(position).getTimestamp();

        Calendar calendar=Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(time));
        String dateTime= DateFormat.format("dd/MM/yyyy  \nhh:mm aa", calendar).toString();

        holder.messageInChat.setText(message);
        holder.timeMessInChat.setText(dateTime);

        if (position==chatList.size()-1){
            if(chatList.get(position).isSeen()) {
                holder.isSeen.setText("✓✓");
            }else{
                holder.isSeen.setText("✓");
            }}else{
            holder.isSeen.setVisibility(View.GONE);

        }




    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    @Override
    public int getItemViewType(int position) {
        firebaseUser= FirebaseAuth.getInstance().getCurrentUser();
        if(chatList.get(position).getSender().equals(firebaseUser.getUid())){
            return MSG_TYPE_RIGHT;
        }
        else
            return MSG_TYPE_LEFT;
    }

    class MyHolder extends RecyclerView.ViewHolder{
        TextView messageInChat, timeMessInChat, isSeen;


        public MyHolder(@NonNull View itemView) {
            super(itemView);

            messageInChat=itemView.findViewById(R.id.message_in_chat);
            timeMessInChat=itemView.findViewById(R.id.time_message_in_chat);
            isSeen=itemView.findViewById(R.id.delivered_messege_in_chat);


        }
    }

}
