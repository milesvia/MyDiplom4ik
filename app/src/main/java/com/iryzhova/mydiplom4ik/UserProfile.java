package com.iryzhova.mydiplom4ik;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.iryzhova.mydiplom4ik.pojo.ItemNavigation;

public class UserProfile extends AppCompatActivity {

    FirebaseAuth firebaseAuth;
    TextView textViewLoginUser;
    MaterialButton buttonOpenContacts;

    private String[] mItemTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerListView;
    private Toolbar mToolbar;
    private CharSequence mTitle;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        firebaseAuth=FirebaseAuth.getInstance();

        textViewLoginUser=findViewById(R.id.textViewLoginUser);

        buttonOpenContacts=findViewById(R.id.buttonOpenContacts);
        buttonOpenContacts.setOnClickListener(v->{

        });

    }

    private void checkUserStatus(){
        FirebaseUser user=firebaseAuth.getCurrentUser();
        if(user==null){
            startActivity(new Intent(UserProfile.this, MainActivity.class));
            finish();
        }
        else {textViewLoginUser.setText(user.getEmail());}
    }

    @Override
    protected void onStart(){
        checkUserStatus();
        super.onStart();
    }

    /*@Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
       if (item.getItemId() == R.id.action_my_profule){
            Toast.makeText(this, "Welcome", Toast.LENGTH_SHORT).show();
        }
        if (item.getItemId() == R.id.action_opencontacts){
            Toast.makeText(this, "Open Contact", Toast.LENGTH_SHORT).show();
        }
        if (item.getItemId() == R.id.action_logout) {
            firebaseAuth.signOut();
            checkUserStatus();
        }
        return true;
    }*/

   /*@Override
    public boolean onCreateOptionMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_maion, menu);
        return true;
    }

    @Override
    public boolean onOptionItemSelected(MenuItem item){


        int id=item.getItemId();
        if(id==R.id.action_logout){
            firebaseAuth.signOut();
            checkUserStatus();
        }
        return super.onOptionsItemSelected(item);
    }*/

}