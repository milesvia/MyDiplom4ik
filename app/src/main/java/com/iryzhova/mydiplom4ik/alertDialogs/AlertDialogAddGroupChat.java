package com.iryzhova.mydiplom4ik.alertDialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatButton;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.iryzhova.mydiplom4ik.R;
import com.iryzhova.mydiplom4ik.pojo.Contact;
import com.iryzhova.mydiplom4ik.pojo.Group;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;


public class AlertDialogAddGroupChat implements AdapterView.OnItemSelectedListener
{
    private Context context;

    private AppCompatButton button_cancelindialogalert, button_crate_new_group,button_chose_members;
    private EditText edit_text_add_name_group,edit_text_add_description_group;
    //private TextView text_view_count_chose_user;

    private AlertDialog dialog;
    private AlertDialogShowUsers alertDialogShowUsers;

    private ArrayList<String > selectedUsers;


    private FirebaseAuth firebaseAuth;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("CutPasteId")
    public AlertDialogAddGroupChat(Context context) {
        this.context=context;
        alertDialogShowUsers=new AlertDialogShowUsers(context);
        selectedUsers=new ArrayList<>();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogView = inflater.inflate(R.layout.dialog_create_new_group, null);
        builder.setView(dialogView);
        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        button_cancelindialogalert = dialogView.findViewById(R.id.button_cancelindialogalert);
        button_crate_new_group = dialogView.findViewById(R.id.button_crate_new_group);
        button_chose_members  = dialogView.findViewById(R.id.button_chose_members);
        edit_text_add_name_group= dialogView.findViewById(R.id.edit_text_add_name_group);
        edit_text_add_description_group = dialogView.findViewById(R.id.edit_text_add_description_group);
        //text_view_count_chose_user = dialogView.findViewById(R.id.text_view_count_chose_user);

        button_cancelindialogalert.setOnClickListener(v->{
            dialog.cancel();
            edit_text_add_description_group.getText().clear();
            edit_text_add_name_group.getText().clear();
            //text_view_count_chose_user.setText("count: 0");
            selectedUsers.clear();
            alertDialogShowUsers.clearChoseUser();

        });

        button_crate_new_group.setOnClickListener(v->createNewGroup());
        button_chose_members.setOnClickListener(v->{ alertDialogShowUsers.showAlertDialogShowUsers(); });

        firebaseAuth=firebaseAuth.getInstance();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void createNewGroup() {
        String nameGroup=edit_text_add_name_group.getText().toString();
        String description=edit_text_add_description_group.getText().toString();
        if(nameGroup.equals("")){
            edit_text_add_name_group.setError("This name is very strange or empty :(");
            edit_text_add_name_group.setFocusable(true);
            return;
        }
        if(description.equals("")){
            edit_text_add_description_group.setError("This description is very strange or empty :(");
            edit_text_add_description_group.setFocusable(true);
            return;
        }
        selectedUsers=getStringUidFromListContact(alertDialogShowUsers.getSelectMembers());
        selectedUsers.add(firebaseAuth.getCurrentUser().getUid());

        HashMap<String, String> hashMap=new HashMap<>();
        String timeTamp=String.valueOf(System.currentTimeMillis());
        hashMap.put("groupId",timeTamp );
        hashMap.put("groupTitle", nameGroup);
        hashMap.put("groupDescription", description);
        hashMap.put("groupIcon", "");
        hashMap.put("timestamp", timeTamp);
        hashMap.put("createBy", firebaseAuth.getUid());
        hashMap.put("uidMembers", selectedUsers.stream().collect(Collectors.joining(",")));

        DatabaseReference ref= FirebaseDatabase.getInstance().getReference("Groups");
        ref.child(timeTamp).setValue(hashMap)
                .addOnSuccessListener(unused -> {
                    Toast.makeText(context, "Successful creating conference", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                    edit_text_add_name_group.getText().clear();
                    edit_text_add_name_group.getText().clear();


                })
                .addOnFailureListener(e -> Toast.makeText(context, " "+e.getMessage(), Toast.LENGTH_LONG).show());

        alertDialogShowUsers.clearChoseUser();
    }

    private ArrayList<String> getStringUidFromListContact(ArrayList<Contact> listContact){
        ArrayList<String> listUid=new ArrayList<>();
        for(Contact contact:listContact){
            listUid.add(contact.getUid());
        }
        return listUid;
    }

    public void showAlertDialogCreateNewGroup() {
        dialog.show();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}




