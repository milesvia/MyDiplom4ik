package com.iryzhova.mydiplom4ik.alertDialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iryzhova.mydiplom4ik.R;
import com.iryzhova.mydiplom4ik.adapter.conference.ConferenceListAdapter;
import com.iryzhova.mydiplom4ik.adapter.showUsers.UsersListsAlertDialogAdapter;
import com.iryzhova.mydiplom4ik.adapter.teteatetechat.ContactsListAdapter;
import com.iryzhova.mydiplom4ik.pojo.Contact;

import java.util.ArrayList;

public class AlertDialogShowUsers {
    private Context context;
    private AppCompatButton button_add_chose_users_indialogalert, button_cancelindialogalert_show;
    private RecyclerView id_recycler_users;

    private AlertDialog dialog;
    private FirebaseAuth firebaseAuth;

    private UsersListsAlertDialogAdapter adapter;

    private ArrayList<Contact> contacts;
    private ArrayList<CheckBox> listCheckBox;

    private EditText edit_text_search_users;

    public AlertDialogShowUsers(Context context) {
        this.context = context;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogView = inflater.inflate(R.layout.dialog_show_all_users, null);
        builder.setView(dialogView);
        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        edit_text_search_users = dialogView.findViewById(R.id.edit_text_search_users);
        button_add_chose_users_indialogalert = dialogView.findViewById(R.id.button_add_chose_users_indialogalert);
        button_cancelindialogalert_show = dialogView.findViewById(R.id.button_cancelindialogalert_show);
        id_recycler_users = dialogView.findViewById(R.id.id_recycler_users);
        id_recycler_users.setHasFixedSize(true);
        initRecycler();
        getAllUsers();


        button_cancelindialogalert_show.setOnClickListener(v->{
            clearChoseUser();
        });

        button_add_chose_users_indialogalert.setOnClickListener(v->{
            choseContact=adapter.getChoseContact();
            System.out.println("================= "+choseContact.size());
            dialog.cancel();
            adapter.falseAllCheckBox();
        });

        firebaseAuth=firebaseAuth.getInstance();

    }
    ArrayList<Contact> choseContact;
    private void initRecycler(){
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(context);
        id_recycler_users.setHasFixedSize(true);
        id_recycler_users.setLayoutManager(linearLayoutManager);
    }

    public void clearChoseUser(){
        dialog.cancel();
        if(choseContact!=null)
            choseContact.clear();
        adapter.falseAllCheckBox();

    }

    private void getAllUsers() {
        contacts = new ArrayList<>();
        FirebaseUser fUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                contacts.clear();
                for (DataSnapshot ds : snapshot.getChildren()) {
                    Contact contact = ds.getValue(Contact.class);
                    if (!contact.getUid().equals(fUser.getUid())) {
                        contacts.add(contact);
                    }
                    adapter = new UsersListsAlertDialogAdapter(context,contacts);
                    id_recycler_users.setAdapter(adapter);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    public void showAlertDialogShowUsers() {
        dialog.show();
    }

    public ArrayList<Contact> getSelectMembers(){
        System.out.println("================= "+choseContact.size());
        return choseContact;
    }
}
