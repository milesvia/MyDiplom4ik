package com.iryzhova.mydiplom4ik.alertDialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.widget.AppCompatButton;

import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.iryzhova.mydiplom4ik.R;

public class ALertDialogSettingsConference {
    private Context context;
    AppCompatButton button_show_people,button_add_person,button_delete_person,button_change_tittle_group, button_cancelindialogalert_set;

    private AlertDialog dialog;
    private FirebaseAuth firebaseAuth;
    private AlertDialogShowUsers alertDialogShowUsers;

    public ALertDialogSettingsConference(Context context) {
        this.context = context;
        alertDialogShowUsers=new AlertDialogShowUsers(context);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogView = inflater.inflate(R.layout.dialog_setting_group, null);
        builder.setView(dialogView);
        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        button_show_people = dialogView.findViewById(R.id.button_show_people);
        button_add_person = dialogView.findViewById(R.id.button_add_person);
        button_delete_person = dialogView.findViewById(R.id.button_delete_person);
        button_change_tittle_group = dialogView.findViewById(R.id.button_change_tittle_group);
        button_cancelindialogalert_set = dialogView.findViewById(R.id.button_cancelindialogalert_set);

        button_show_people.setOnClickListener(v->{ alertDialogShowUsers.showAlertDialogShowUsers(); });
        button_cancelindialogalert_set.setOnClickListener(v->{ dialog.cancel(); });

        firebaseAuth=firebaseAuth.getInstance();
    }

    public void showALertDialogSettingsConference() {
        dialog.show();
    }
}
